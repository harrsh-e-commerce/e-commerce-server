import Express from './providers/express';
import Server from './providers/server';
import logger from './providers/logger';
import connectDB from './providers/db';

const StartServer = async () => {
    const express = Express();

    express.initializeApp();
    express.configureViews();

    const app = express.app;

    const httpServer = Server(app);
    await httpServer.start();

    await connectDB();
};

StartServer();

process.on('uncaughtException', (err) => {
    logger.error(err);
    process.exit(1);
});

process.on('SIGTERM', async () => {
    logger.debug('SIGTERM signal received: closing HTTP server');
    process.exit(1);
});

process.on('unhandledRejection', (err) => {
    logger.error(err);
    process.exit(1);
});
