import { Request, Response } from 'express';
import response from '../../../../libs/response';
import {
    AdminFindOne,
    AdminUpdateOneById,
} from '../../../service/admin.service';
import { comparePassword } from '../../../utils/managePassword';
import { generateOTP } from '../../../utils/otp';
import { Admin_SignIn_RequestType } from '../../../requests/admin/auth.request';

/**
 * Sign in for admin.
 * @url     /admin/auth/signin
 * @access  Public
 * @method  POST
 */
const Admin_SignIn_Controller = async (req: Request, res: Response) => {
    try {
        const {
            body: { email, password },
        }: Admin_SignIn_RequestType = req.body.parsedData;

        const admin = await AdminFindOne({
            email: {
                email,
            },
        });

        const doesPasswordMatch = await comparePassword(
            password,
            admin.password.password
        );
        if (!doesPasswordMatch) {
            throw new Error('Your password does not match.');
        }

        const otp = generateOTP();
        await AdminUpdateOneById(admin.id, {
            $set: {
                'loginOtp.otp': otp,
                'loginOtp.otpSentAt': new Date(),
            },
        });

        return response(req, res, {
            message: 'OTP has been sent to your contact number.',
        });
    } catch (error) {
        return response(req, res, null, error);
    }
};

export default Admin_SignIn_Controller;
