import { Request, Response } from 'express';
import logger from '../../providers/logger';

const Ping = async (req: Request, res: Response) => {
    try {
        return res.json({
            message: 'Pong',
        });
    } catch (e) {
        logger.error(e);
    }
};

export default Ping;
