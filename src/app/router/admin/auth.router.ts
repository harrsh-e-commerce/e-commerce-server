import { Router } from 'express';
import Admin_ForgotPassword_Controller from '../../controllers/admin/auth/forgotPassword.controller';
import Admin_SignIn_Controller from '../../controllers/admin/auth/signIn.controller';
import Admin_VerifySignInOtp_Controller from '../../controllers/admin/auth/verifySignInOtp.controller';
import requestValidator from '../../middlewares/requestValidator';
import {
    Admin_ForgotPassword_Request,
    Admin_SignIn_Request,
    Admin_VerifySignInOtp_Request,
} from '../../requests/admin/auth.request';

const AuthRouter = Router();

AuthRouter.post(
    '/signin',
    requestValidator(Admin_SignIn_Request),
    Admin_SignIn_Controller
);

AuthRouter.post(
    '/verify-signin-otp',
    requestValidator(Admin_VerifySignInOtp_Request),
    Admin_VerifySignInOtp_Controller
);

AuthRouter.post(
    '/forgot-password',
    requestValidator(Admin_ForgotPassword_Request),
    Admin_ForgotPassword_Controller
);

export default AuthRouter;
