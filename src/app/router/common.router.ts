import { Router } from 'express';
import requestValidator from '../middlewares/requestValidator';
import { ResetPassword_Request } from '../requests/common.request';
import ResetPassword_Controller from '../controllers/common/resetPassword.controller';

const CommonRouter = Router();

CommonRouter.post(
    '/reset-password',
    requestValidator(ResetPassword_Request),
    ResetPassword_Controller
);

export default CommonRouter;
