import { Router } from 'express';
import Ping from '../controllers';
import AdminRouter from './admin';
import CommonRouter from './common.router';

const router = Router();

router.get('/', Ping);

// Admin routes
router.use('/admin', AdminRouter);

// Common routes
router.use('/', CommonRouter);

export default router;
