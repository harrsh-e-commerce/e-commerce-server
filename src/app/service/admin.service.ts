import { FilterQuery, UpdateQuery } from 'mongoose';
import Admin, { IAdminSchema } from '../schemas/admin.schema';

export const AdminFindOne = async (args: FilterQuery<IAdminSchema>) => {
    const admin = await Admin.findOne({
        ...args,
        deletedAt: null,
    });

    if (!admin) {
        throw new Error('Admin not found.');
    }

    return admin;
};

export const AdminFindById = async (id: string) => {
    const admin = await Admin.findById(id);

    if (!admin || admin.deletedAt != null) {
        throw new Error('Admin not found.');
    }

    return admin;
};

export const AdminUpdateOneById = async (
    id: string,
    args: UpdateQuery<IAdminSchema>
) => {
    await AdminFindById(id);

    const admin = await Admin.findByIdAndUpdate(id, args, {
        new: true,
    });

    if (!admin || admin.deletedAt != null) {
        throw new Error('Admin not found.');
    }

    return admin;
};

export const AdminDeleteMany = async (args?: FilterQuery<IAdminSchema>) => {
    await Admin.deleteMany(args);

    return true;
};
