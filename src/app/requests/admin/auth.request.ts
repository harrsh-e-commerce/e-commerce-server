import { object, string, z } from 'zod';

// Sign in for admin.
export const Admin_SignIn_Request = object({
    body: object({
        email: string({
            required_error: 'Email is required.',
        })
            .nonempty('Email is required.')
            .email('Not a valid email.'),
        password: string({
            required_error: 'Password is required.',
        }).nonempty('Password is required.'),
    }),
});
export type Admin_SignIn_RequestType = z.infer<typeof Admin_SignIn_Request>;

// Verify OTP for admin sign in.
export const Admin_VerifySignInOtp_Request = object({
    body: object({
        email: string({
            required_error: 'Email is required.',
        })
            .nonempty('Email is required.')
            .email('Not a valid email.'),
        otp: string({
            required_error: 'OTP is required.',
        }).nonempty('OTP is required.'),
    }),
});
export type Admin_VerifySignInOtp_RequestType = z.infer<
    typeof Admin_VerifySignInOtp_Request
>;

// Verify OTP for admin sign in.
export const Admin_ForgotPassword_Request = object({
    body: object({
        email: string({
            required_error: 'Email is required.',
        })
            .nonempty('Email is required.')
            .email('Not a valid email.'),
    }),
});
export type Admin_ForgotPassword_RequestType = z.infer<
    typeof Admin_ForgotPassword_Request
>;
