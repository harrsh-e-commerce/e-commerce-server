import { object, string, z } from 'zod';

// Reset password for all users.
export const ResetPassword_Request = object({
    body: object({
        password: string({
            required_error: 'Password is required.',
        }).nonempty('Password is required.'),
        token: string({
            required_error: 'Token is required.',
        }).nonempty('Token is required.'),
    }),
});
export type ResetPassword_RequestType = z.infer<typeof ResetPassword_Request>;
