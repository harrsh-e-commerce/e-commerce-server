import mongoose, { Document, model } from 'mongoose';

const { Schema } = mongoose;

const adminSchema = new Schema<IAdminSchema>(
    {
        name: {
            type: String,
            required: true,
        },
        email: {
            email: {
                type: String,
                required: true,
                unique: true,
            },
            newEmail: {
                type: String,
            },
            emailVerificationToken: {
                type: String,
            },
            emailVerifiedAt: {
                type: Date,
            },
        },
        contactNumber: {
            contactNumber: {
                type: String,
                required: true,
                unique: true,
            },
            newContactNumber: {
                type: String,
            },
            contactNumberVerificationToken: {
                type: String,
            },
            contactNumberVerifiedAt: {
                type: Date,
            },
        },
        password: {
            password: {
                type: String,
                required: true,
            },
            passwordVerificationToken: {
                type: String,
            },
        },
        loginOtp: {
            otp: String,
            otpSentAt: Date,
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        deletedAt: {
            type: Date,
        },
    },
    {
        timestamps: true,
    }
);

const Admin = model<IAdminSchema>('Admin', adminSchema);

export default Admin;

export interface IAdminSchema extends Document {
    name: string;
    email: {
        email: string;
        newEmail?: string;
        emailVerificationToken?: string;
        emailVerifiedAt?: Date;
    };
    contactNumber: {
        contactNumber: string;
        newContactNumber?: string;
        contactNumberVerificationToken?: string;
        contactNumberVerifiedAt?: Date;
    };
    password: {
        password: string;
        passwordVerificationToken?: string;
    };
    loginOtp: {
        otp?: string;
        otpSentAt?: Date;
    };
    isActive: Boolean;
    deletedAt?: Date;
}
