import { Document } from 'mongoose';
import { IAdminSchema } from '../schemas/admin.schema';
import { Types } from 'mongoose';

const cleanAdminData = (
    admin: Document<unknown, {}, IAdminSchema> &
        Omit<
            IAdminSchema & {
                _id: Types.ObjectId;
            },
            never
        >
) => {
    const cleanAdmin = {
        _id: admin._id,
        name: admin.name,
        email: admin.email.email,
        newEmail: admin.email.newEmail,
        contactNumber: admin.contactNumber.contactNumber,
        isActive: admin.isActive,
    };

    return cleanAdmin;
};

export default cleanAdminData;
