import dayjs from 'dayjs';
import { QUnitType } from 'dayjs';
import { OpUnitType } from 'dayjs';

export const getTimeDifference = (
    startDateString: string | Date,
    endDateString: string | Date = new Date(),
    unit: QUnitType | OpUnitType = 'minute'
) => {
    const startDate = dayjs(startDateString);
    const endDate = dayjs(endDateString);

    return startDate.diff(endDate, unit);
};
