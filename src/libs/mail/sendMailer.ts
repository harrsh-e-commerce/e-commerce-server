import { TemplateOptions } from 'nodemailer-express-handlebars';
import Mail from 'nodemailer/lib/mailer';
import transporter from '.';
import env from '../../env';
import logger from '../../providers/logger';

const sendMailer = async (props: SendMailerPropTypes): Promise<void> => {
    const { subject, to_address, context, template } = props;

    console.log('Props are >> ', {
        data: props,
    });

    try {
        const options: Mail.Options & TemplateOptions = {
            from: env.mail.email_from,
            to: to_address,
            subject,
            template,
            context,
        };

        const response = await transporter.sendMail(options);
        logger.info(response);

        return Promise.resolve();
    } catch (error: any) {
        Promise.reject(error.message);
    }
};

export default sendMailer;

export interface SendMailerPropTypes {
    subject: string | undefined;
    to_address: string | Mail.Address | (string | Mail.Address)[] | undefined;
    context: any;
    template: string | undefined;
}
